""" Contains functions used in training the models for COICOP classification
Written by Nick de Wolf and Lanthao Benedikt
"""

import os
import time
from pathlib import Path
from matplotlib.pyplot import grid, sca
import pandas as pd
import numpy as np
import joblib
from datetime import datetime
import importlib
import numpy as np
import json

from sklearn import ensemble
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn import linear_model, naive_bayes
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV

from sklearn.pipeline import Pipeline, make_pipeline
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import balanced_accuracy_score

import fasttext
import fasttext_classifier
from fasttext_extractor import UnityExtractor

import matplotlib.pylab as plt

from coicop_preprocessing import CoicopPreprocessing
from coicop_helper import CoicopHelper
import tuning

import logging

class CoicopClassifier:
    """Read labelled data, merge them into a single dataframe and clean data.

    Attributes:
        var: Column name of the input data.
        label: Column name of the output label.
        train_test_proportion: float percentage of the data used for training.
        model_dir: Path to the folder in which the trained models have to be stored.
        result_dir: Path to the folder in which the results have to be stored.
        dict_f_extract: Dictionary with different features to be tested.
        dict_clf: Dictionary with the different classifiers to be trained.
        label_encoder: The Label encoder used with the models.

    """

    def __init__(self, var, label, model_dir, result_dir, set_split = 0.8, cv = 5):
        """[summary]

        Args:
            var (string): Column name of the input data.
            label (string): Column name of the output label.
            model_dir (string): Path to the folder in which the trained models have to be stored.
            result_dir (string): Path to the folder in which the results have to be stored.
            set_split (float, optional): float percentage of the data used for training.
        """
        self.var = var
        self.label = label
        self.train_test_proportion = set_split   # Split training set 80% / test set 20%
        self.model_dir = model_dir  # Where we output the trained models
        self.result_dir = result_dir # Where we output the prediction results

        if model_dir:
            Path(model_dir).mkdir(parents=True, exist_ok=True)

        if result_dir:
            Path(result_dir).mkdir(parents=True, exist_ok=True)

        self.dict_f_extract = {'CountVect': CountVectorizer(analyzer='word', token_pattern=r'\w{2,}', max_features=5000),
                               'TFIDF_word': TfidfVectorizer(analyzer='word', token_pattern=r'\w{2,}', max_features=5000),
                               'TFIDF_char': TfidfVectorizer(analyzer='char', token_pattern=r'\w{2,}', ngram_range=(2,3), max_features=5000),
                               'TFIDF_char34': TfidfVectorizer(analyzer='char', token_pattern=r'\w{2,}', ngram_range=(3,4), max_features=5000),
                               'Count_char': CountVectorizer(analyzer='char', token_pattern=r'\w{2,}', max_features=5000)
                               }
        self.dict_clf = {
                        'FT': fasttext_classifier.FasttextEstimator(),
                        'NB': naive_bayes.MultinomialNB(),
                        'LR': linear_model.LogisticRegression(),
                        'RF': ensemble.RandomForestClassifier(n_estimators=500, max_depth=20, n_jobs=12),
                        'DT': DecisionTreeClassifier(),
						'SGD': SGDClassifier(loss="log")
                         }
        self.label_encoder = LabelEncoder()
        self.xtrain = None
        self.df_data = None
        self.y_train = None
        self.x_test = None
        self.y_test = None
        self.cv = cv

    def split_data(self, data_df, remove_below_limit=5):
        # First lines of build_and_validate_models. For tuning hyperparameters, splitting and preprocessing should 
        # only performed once. 

        # Split stratified into training set / test set
        # We assume tokenized input is presented to the algorithm.
        self.data_df, _ = CoicopPreprocessing.filter_rare_items(data_df, self.label, remove_below_limit)
        self.x_train, self.x_test, self.y_train, self.y_test = self.split_stratified(self.data_df, seed_split=23)

        return self.data_df, self.x_train, self.x_test, self.y_train, self.y_test


    def build_and_validate_models(self, store_model = False):
        """Calls the pipeline to build and validate the models as specified during initialization. 
        The method uses data from instance variables. It is possible to overwrite the instance variables using e.g., the method split_data or split_stratified.

        Args:
            remove_below_limit (int, optional): Minimum amount that a labels has to occur in the training set. Defaults to 5.

        Returns:
            (pandas.DataFrame, pandas.DataFrame): Return tuple of the combined results for the test set
        """

        statistics_list = []
        all_result_df = pd.DataFrame()
        all_result_df['RECDESC'] = self.x_test
        all_result_df['Ground_truth'] = self.label_encoder.inverse_transform(self.y_test)

        # ---------------------- #
        # Train the models
        #
        # Validate the models: make a dataframe with all results for all models and save to drive
        #         The dataframe contains receipt desc, ground truth, predictions, proba for each model
        # ---------------------- #
        logging.info('\nTRAINING SUPERVISED MODELS...')

        # Train and validate all models for all feature extraction methods
        for clf, classifier in self.dict_clf.items(): # Loop through the list of models we want to train
            if clf == "FT":  # FastText requires special features and labels
                feature = 'unity'
                extractor = UnityExtractor()
                ytest_FT = fasttext_classifier.preprocess_labels(self.y_test)
                ytrain_FT = fasttext_classifier.preprocess_labels(self.y_train)

                pipeline = self.train_model(self.x_train, ytrain_FT, feature, extractor, clf, classifier, store_model = store_model)    
                tmp_df, statistics = self.validate_model(self.x_test, ytest_FT, clf, feature, pipeline)
                statistics_list += [statistics]
                if len(tmp_df) > 0:
                    logging.debug(tmp_df.head(10))
                    all_result_df = pd.concat([all_result_df, tmp_df], axis=1)

            else: 
                for feature, extractor in self.dict_f_extract.items(): # Loop through the list of feature extraction
                    logging.debug('\t... with feature extraction method=%s, vectorizer=%s' % (feature, extractor))
                    pipeline = self.train_model(self.x_train, self.y_train, feature, extractor, clf, classifier, store_model = store_model)
                    tmp_df, statistics = self.validate_model(self.x_test, self.y_test, clf, feature, pipeline)

                    statistics_list += [statistics]
                    if len(tmp_df) > 0:
                        logging.debug(tmp_df.head(10))
                        all_result_df = pd.concat([all_result_df, tmp_df], axis=1)

        statistics_df = self._build_statistics_from_lists(statistics_list)

        # Store results
        result_file_name = self.result_dir + 'validation_results.csv'
        result_file_name = self.result_dir + 'statistics_results.csv'
        logging.info('Saving final results to %s'%result_file_name)
        all_result_df.to_csv(result_file_name, index=False)
        statistics_df.to_csv(result_file_name, index=False)

        return all_result_df, statistics_df

    def tune_model(self, params, clfname, featname, method = 'grid_search', nruns = 100, save_model = True):
        """ Tune one or more models given a set of hyperparameters. Feature extraction method defined by instance variable.

        Args:
            params (dict): hyperparameters to be tuned with values as input for method
            clfname (str): name of the algorithm to train/predict
            featname (str): name of feature extraction method
            method (str): method to optimize hyperparameters
            save_model (bool): whether to save the best model

        Returns: (DataFrame with statistics, parameters corresponding to the statistics)
        """

        # initialize result
        statistics_list = []

        # tuning.check_prefix_params(params, type = 'model') # tune only model currently  

        # preprocess parameters
        if clfname == 'FT': # todo: for other methods
            if params != None:
                if len(params) == 0:
                    # set default parameters
                    params = {'epoch':[20], 'lr':[1.0], 'wordNgrams':[3], 'minCount':[1], 'minn':[3], 'maxn':[12]}
                else:
                    valid_params = tuning.check_prefix_params(params)
                    if not valid_params:
                        raise ValueError("invalid parameters.")


        classifier = self.dict_clf[clfname]
        if clfname == "FT":  # FastText requires special features and labels
            extractor = UnityExtractor()
            y_test = fasttext_classifier.preprocess_labels(self.y_test)
            y_train = fasttext_classifier.preprocess_labels(self.y_train)

        else:
            extractor = self.dict_f_extract[featname]
            y_test = self.y_test
            y_train = self.y_train

        # initialize hyperparameter search
        scikit_pipeline = Pipeline([(featname, extractor),
                                    (clfname, classifier)])
        if method == 'grid_search':
            # grid search of at least one run
            params_per_run = tuning.prepare_gridsearch(params)
            nruns = len(params_per_run.keys())
            logging.debug("Starting GridSearch with " + str(nruns) + " iterations and the following parameters:")
            logging.debug(params)

            hyperparclf = GridSearchCV(scikit_pipeline, 
                                    param_grid = params, 
                                    cv = self.cv,
                                    refit = True, # necessary for predict,
                                    scoring = 'accuracy',  # 'f1_macro',
                                    verbose = 3)

        elif method == 'random_search':
            logging.debug("Starting RandomSearch with " + str(nruns) + " iterations and the following parameter intervals")
            logging.debug(params)

            hyperparclf = RandomizedSearchCV(scikit_pipeline, 
                        param_distributions = params, 
                        cv = self.cv,
                        n_iter = nruns,
                        refit = True, # necessary for predict
                        scoring = 'accuracy', 
                        random_state = 12345,
                        verbose = 3)
        else:
            raise ValueError("Method not implemented")

        # fit estimators
        hyperparclf.fit(self.x_train, y_train)

        # store results
        if save_model:
            if clfname == "FT":
                self.savemdl(hyperparclf.best_estimator_['FT'], clfname)
            else:
                self.savemdl(hyperparclf.best_estimator_, clfname)

        logging.debug("Best parameters set found on development set:\n")
        logging.debug(hyperparclf.best_params_)
        logging.debug("Grid scores on development set:")
        means = hyperparclf.cv_results_['mean_test_score']
        stds = hyperparclf.cv_results_['std_test_score']
        for mean, std, params in zip(means, stds, hyperparclf.cv_results_['params']):
            logging.debug("%0.3f (+/-%0.03f) for %r" % (mean, std * 2, params))

        logging.debug("Detailed classification report:")
        logging.debug("The model is trained on the full development set.")
        logging.debug("The scores are computed on the full evaluation set.")
        y_true, y_pred = y_test, hyperparclf.predict(self.x_test)

        # Store results: dict with numpy arrays
        result_file_name = os.path.join(self.result_dir, f'tuning_results_{method}_{clfname}.npy')
        logging.info('Saving final results to %s' %result_file_name)
        np.save(result_file_name, hyperparclf.cv_results_)

        return hyperparclf.cv_results_, hyperparclf.best_params_


    def split_stratified(self, df_data, seed_split=23):
        """Split the data in a stratified way. And saves the label encoder

        Args:
            df_data (pandas.DataFrame): DataFrame with input/output

        Returns:
            (list, list, list, list, list, list): Return stratified split for x, y
        """

        # x: Take the whole dataframe except the label column (i.e. RECDESC and column prepared for FastText
        x = df_data[self.var]
        # y: label column used for stratification
        y = df_data[self.label]
        # Do the split
        x_all_train, x_all_test, y_train, y_test = train_test_split(x, y,
                                                           train_size=self.train_test_proportion,
                                                           random_state=seed_split,
                                                           stratify=y)
        # -------------------------------------------------------------------------- #
        # Separate the data columns used for Scikit-learn and the data for FastText
        # -------------------------------------------------------------------------- #
        # These are for Scikit-Learn: take the columns and apply label encoding on the label colum
        if x_all_train.ndim == 1:  # Pandas Series
            self.x_train = x_all_train.tolist()
        else:  # Pandas DataFrame   
            self.x_train = list(x_all_train[:, 0])
        if x_all_test.ndim == 1:  # Pandas Series
            self.x_test = x_all_test.tolist() 
        else:  # Pandas DataFrame
            self.x_test = list(x_all_test[:,0])
        self.y_train = self.label_encoder.fit_transform(y_train)
        self.y_test = self.label_encoder.transform(y_test)
        # Examine the classes of the fitted LabelEncoder.
        logging.debug(f'Number of classes of the LabelEncoder = {len(self.label_encoder.classes_)}: \nThe classes:')
        logging.debug(list(self.label_encoder.classes_))
        # Save the Label Encoder
        encoder_filename = self.model_dir + 'Label_Encoder.bin'
        logging.info(f'Save Label Encoder to: {encoder_filename}\n')
        joblib.dump(self.label_encoder, encoder_filename)
        # Checking the data size...
        logging.info(f'Trainingset: {len(self.x_train)} rows')
        logging.info(f'Testset: {len(self.x_test)} rows')
        # Return train features, test features, train classes, test_classes
        return self.x_train, self.x_test, self.y_train, self.y_test

    def train_model(self, x_train, y_train, featname, extractor, clfname, classifier, store_model = False):
        """Fits a scikit model to the data.
        Training features and labels need to be specified, as requirements on the format can differ per algorithm

        Args:
            x_train (list): List with input data.
            y_train (list): List with labels.
            featname (string): Feature name.
            extractor (object): Feature representation instance.
            clfname (string): Name of the classifier.
            classifier (object): Classifier instance.
            save_model (bool): whether to save the trained model.

        Returns:
            object: Trained Sklearn Pipeline (model).
        """
        filename = self.model_dir + clfname + '_' + featname + '_tst.bin'

        if Path(filename).is_file():
            logging.info('Classifier: %s, feature: %s. A pre-trained model already exists. Please delete it from the drive if you wish to train a new one'%(clfname,featname))
            scikit_pipeline = joblib.load(filename)
        else:
            logging.info('Currently training classifier: %s, feature: %s.' % (clfname, featname))

            # Define the pipeline and train the model:
            if clfname == 'FT':
                scikit_pipeline = classifier.fit(x_train, y_train)
            else:
                scikit_pipeline = Pipeline([(featname, extractor),
                                            (clfname, classifier)])
                scikit_pipeline.fit(x_train, y_train)
            
            logging.info(scikit_pipeline)

            # Store the resulting model
            if store_model:
                self.savemdl(scikit_pipeline, clfname, fname = "coicop_output/trained_model.bin")
                
        return scikit_pipeline

    def validate_model(self, x_test, y_test, clfname, featname, pipeline=None):
        """Use models to predict items in testset then compare to the groundtruth.
        Training features and labels need to be specified, as requirements on the format can differ per algorithm

        Args:
            x_test (list): List with the strings for input data
            y_test (list): List with the labels
            clfname (string): Classifier name
            featname (string): Name of the used features
            pipeline (object, optional): Use this object binary if present, else load from file.

        Returns:
            pandas.DataFrame: Output of the model
        """
        df = pd.DataFrame()
        if not pipeline:
            model_filename = self.model_dir + clfname + '_' + featname + '.bin'
            pipeline = CoicopHelper.load_model(model_filename)
        predict_result = pipeline.predict(x_test)
        predict_proba = pipeline.predict_proba(x_test)

        if isinstance(predict_proba, list):
            predict_proba = np.array(predict_proba)
        if predict_proba.shape[1] == 1:
            # only top scores are returned
            maxscores = predict_proba.flatten()
        else:
            # all scores are returned, pick maximum 
            maxscores = (predict_proba[np.arange(len(predict_proba)), np.argmax(predict_proba, axis=1)])
        
        if isinstance(predict_result, list):
            predict_result = np.array(predict_result)
        if predict_result.ndim > 1:
            if predict_result.shape[1] == 1:
                # only top scores are returned
                predict_result = predict_result.flatten()

        # undo label processing if necessary
        ft_labels = fasttext_classifier.check_labels(y_test)
        if ft_labels:
            y_test = fasttext_classifier.undo_label_preprocessing(y_test)
            predict_result = fasttext_classifier.undo_label_preprocessing(predict_result)

        df['RECDESC'] = x_test
        df['Ground_truth'] = self.label_encoder.inverse_transform(y_test)
        df[clfname + '_' + featname] = self.label_encoder.inverse_transform(predict_result)
        df[clfname + '_' + featname + '_Prob'] = maxscores
        df[clfname + '_' + featname + '_Prob'] = df[clfname + '_' + featname + '_Prob'].apply(lambda row: int(round(row * 100)))
        df[clfname + '_' + featname + '_Status'] = np.where(df['Ground_truth'] == df[clfname + '_' + featname], 1, 0)
        statistics = self.show_results(clfname + '_' + featname, df[clfname + '_' + featname], df['Ground_truth'])
        return df[[clfname + '_' + featname,
                   clfname + '_' + featname + '_Prob',
                   clfname + '_' + featname + '_Status']], statistics

    # ------------------------------------- #
    # loading/saving/getters/setters

    def savemdl(self, pipeline, clf, fname = None):
        """ Store model using joblib

        Args:
            pipeline (any python object): a model or pipeline
            fname (string): optional parameter containing name of file.
        """
        if fname is None:
            model_version = datetime.now().strftime("%Y%m%d_%H%M%S")
            fname = os.path.join(self.model_dir, model_version + '.bin')
        logging.info('\tSave model to: %s\n' % fname)

        if clf == "FT":
            pipeline.save_mdl(fname)
        else:
            joblib.dump(pipeline, fname)

    def loadmdl(self, fname, clf):
        """ Read a model from model directory
        Test if FT model is loaded correctly via e.g., print(model.words)

        Args:
            fname (string): name of the model to be loaded
        """
        if clf == 'FT':
            self.model = fasttext.load_model(os.path.join(self.model_dir, fname))
        else:
            self.model = joblib.load(os.path.join(self.model_dir, fname))

        return self.model

    def set_params(self, params):
        """ Set parameters for models, to be used for example after optimizing hyperparameters
        Args:   
            params (dict): keys are names of models, values are dict of parameters and associated values
        """
        for clf in params.keys():
            if clf in self.dict_clf.keys():
                try:
                    self.dict_clf[clf].set_params(**params[clf])
                except:
                    logging.error("Setting parameters failed. Below is list of available parameters:")
                    logging.error(self.dict_clf[clf].get_params().keys())
            else:
                raise ValueError(f"Cannot set parameters, model {clf} is unknown.")



    # ------------------------------------- #
    # Plot results
    def plot_and_save_results(self, perf, clfname, featname):
        """Plot results

        Args:
            perf (pandas.DataFrame): Results
            clfname (string): Name of the classifier
            featname (string): Name of the feature
        """
        plt.clf()
        # Status == 1: correctly classified
        perf[perf[clfname + '_' + featname + '_Status'] == 1][clfname + '_' + featname + '_Prob'].hist(bins=50, alpha=0.5, density=False)
        # Status == 0: incorrectly classified
        perf[perf[clfname + '_' + featname + '_Status'] == 0][clfname + '_' + featname + '_Prob'].hist(bins=50, alpha=0.65, density=False)

        plt.xlabel('Confidence')
        plt.ylabel('Number of items')
        plt.title('Characteristic of classifier %s - feature %s'%(clfname, featname))
        filename = self.result_dir + '/' + clfname + '_' + featname
        plt.savefig(filename)
        plt.show()

    def show_results(self, name, y_pred, y_true):
        """Show statistics given the predictions and the true values.

        Args:
            name (string): Name to show with the results
            y_pred (list): Predicted values
            y_true (list): True values

        Returns:
            [type]: [description]
        """
        logging.info(name)
        logging.info(classification_report(y_true, y_pred, digits = 4))
        precision, recall, fscore, _ = precision_recall_fscore_support(y_true, y_pred, average="weighted")
        accuracy = accuracy_score(y_true, y_pred)
        balanced_accuracy = balanced_accuracy_score(y_true, y_pred)
        logging.info("Precision {0:f}, Recall {1:f}, fscore {2:f}".format(precision, recall, fscore))
        logging.info("Accuracy", accuracy)
        logging.info("Balanced Accuracy", balanced_accuracy)
        return [name, precision, recall, fscore, accuracy, balanced_accuracy]

    def _build_statistics_from_lists(self, statistics,):
        """Make a bigger statistics dataFrame.
        TODO: Add more statistics to be generated.

        Args:
            statistics (list): List of list of results per run.

        Returns:
            [type]: [description]
        """
        result = pd.DataFrame(statistics)
        # set column names
        result.columns = ["name", "precision", "recall", "fscore", "accuracy", "balanced_accuracy"]
        logging.debug(f"Result of build_statistics_from_lists: {result}")
        return result

    @staticmethod
    def predict(receipt_df, model, label_encoder, column, coicop_nr_name=None, result_file=None):
        """Classifies a column of a given DataFrame.

        Args:
            receipt_df (pandas.DataFrame): DataFrame containing product descriptions
            column (string): Name of the column to be classified.
            result_file (string, optional): Filename incase the results have to be stored.

        Returns:
            (pandas.DataFrame): DataFrame containing the results of the classification.
        """
        if isinstance(model, str):
            model = CoicopHelper.load_model(model)
        if isinstance(label_encoder, str):
            label_encoder = joblib.load(label_encoder)
        if coicop_nr_name:
            if isinstance(coicop_nr_name, str):
                coicop_nr_name = {row[0]:row[1] for _, row in pd.read_csv(coicop_nr_name).iterrows()}

        clean_df = CoicopPreprocessing.clean_receipt(receipt_df, column)
        # Further steps are not needed if there are no rows.
        if len(clean_df) > 0:
            # Apply same tokenization as during training
            prepped_column = clean_df[column].apply(CoicopPreprocessing.tokenizer)
            predictions = model.predict(list(prepped_column))

            clean_df['Coicop_class'] = pd.DataFrame.from_records(predictions[0])
            if isinstance(model, fasttext.FastText._FastText):
                clean_df['Coicop_class'] = clean_df['Coicop_class'].str.replace('__label__','')
            clean_df['Coicop_class'] = pd.to_numeric(clean_df['Coicop_class'], errors='coerce').fillna(0).astype(np.int64)
            clean_df['Coicop_class'] = label_encoder.inverse_transform(clean_df['Coicop_class'])
            clean_df['Coicop_class'] = clean_df['Coicop_class'].apply(CoicopPreprocessing.add_dots)
            if coicop_nr_name:
                clean_df['Coicop_class_name'] = clean_df['Coicop_class'].replace(coicop_nr_name)
            clean_df['Coicop_class_prob'] = pd.DataFrame.from_records(predictions[1])
            clean_df['Coicop_class_prob'] = clean_df['Coicop_class_prob'].fillna(0).apply(lambda row: int(round(row*100)))
            #clean_df['FastText_ngram_Prob'] = clean_df['FastText_ngram_Prob'].apply(lambda row: int(round(row*100)))
            clean_df = clean_df.drop(["UPC barcode"], axis=1)

        if result_file:
            clean_df.to_csv(result_file, index=False)

        return clean_df
