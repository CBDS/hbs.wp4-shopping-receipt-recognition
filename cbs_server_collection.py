import logging
import datetime
import time
import sys

import pandas as pd

from typing import Iterable

from api_requests import ApiRequests
from data_collection_interface import DataCollectionInterface
from receipt_processed import ReceiptProcessed
from receipt_raw_server import ReceiptRawServer

class CBSServerCollection(DataCollectionInterface):

    def __init__(self, requests_config:str, sync_time:int=0, wait_time:int=300):
        self.api = ApiRequests(requests_config)

        self.api.register_new_phone(
            self.api.config_file["user"]["name"],
            self.api.config_file["user"]["password"],
            self.api.config_file["phone"]["name"] 
        )

        self.start_sync_id = 1
        self.start_sync_time = sync_time
        self.wait_time = wait_time

    def retrieve(self, infinite_loop: bool=False) -> Iterable[ReceiptRawServer]:
        logging.info("Start querying server for new receipts.")
        sync_id = self.start_sync_id
        sync_time = self.start_sync_time
        while sync_time >= self.start_sync_time :
            date_time = str(datetime.datetime.now().strftime('%Y%m%d%H%M%S'))
            logging.info(f"{date_time} Processing receipt: {sync_id}, {sync_time}:")

            receipt = self.api.pull_request(sync_id, sync_time)
            content, image_present, new_sync_id, new_sync_time = self.api.parse_content(receipt)
            if image_present:                
                yield ReceiptRawServer.from_server_request(sync_id, sync_time, content)
            else:
                logging.warning(f"The entry  with sync_time={sync_time} does not contain an image. Skipping.")
            
            # Only possible if database is not setup correctly or the database is empty.
            if sync_time == new_sync_time:
                raise ValueError("The server does not contain any receipts or the server is not set up correctly.")
            
            if infinite_loop:
                if sync_time < self.start_sync_time:
                    date_time = str(datetime.datetime.now().strftime('%Y%m%d%H%M%S'))
                    logging.info(f"{date_time} No new receipts at: {sync_id}, {sync_time}. Reseeting to start pointing and sleeping {self.wait_time} seconds.")
                    sync_time = self.start_sync_time
                    time.sleep(self.wait_time)

            sync_id = new_sync_id
            sync_time = new_sync_time            

    def store(self, processed_receipt: ReceiptProcessed, results: pd.DataFrame):
        output_request = self.api.return_processed_json(processed_receipt.request_content, results)
        phone_request = self.api.pull_phone_request()
        self.api.push_request(output_request)

        current_time = str(datetime.datetime.now().strftime('%Y%m%d%H%M%S'))
        if self.api.pull_phone_request() > phone_request:
            logging.info(f"{current_time} Update request for {processed_receipt.sync_id} successful.")
            return True
        else:
            logging.warning(f"{current_time} Update request for {processed_receipt.sync_id} unsuccessful.")
            return False

if __name__ == "__main__":
    current_time = str(datetime.datetime.now().strftime("%Y%m%d%H%M%S"))
    logging_file = f'hbs_{current_time}.log'
    format = '%(asctime)s | %(name)s |  %(levelname)s: %(message)s'
    formatter = logging.Formatter(format)

    file_handler = logging.FileHandler(logging_file)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setLevel(logging.INFO)
    stdout_handler.setFormatter(formatter)
    
    #logging.basicConfig(handlers=[file_handler, stdout_handler], format=format, level=logging.DEBUG)
    logging.basicConfig(handlers=[file_handler, stdout_handler])
    receiptCollection = CBSServerCollection("./configs/request_config.json")
    for x in receiptCollection.retrieve():
        print(x.sync_time)
    