""" Class involving the general OCR pipeline

Written by Lanthao Benedikt and Nick de Wolf
"""

import re
import pandas as pd

from ocr_abstract_pipeline import AbstractPipeline

pd.set_option('display.width', 400)
pd.set_option('display.max_columns', 10)

class Pipeline(AbstractPipeline):
    """General pipeline for OCR

    Read config and receipt image file. Run OCR and call class Parser to
    Retrieve shopname, date, items descriptions and prices
    """
   
    # ------------------------------------------------------------------------------------ #
    # All functions for splitting lines into barcode/description/price and make df
    # ------------------------------------------------------------------------------------ #

    def _find_amount_price(self, line, search_upc_barcode=True):
        """Find the amount and price on a line in the receipt.

        Args:
            description_str (string): Line on a receipt.

        Returns:
            (string, string, string, int, re.Match, list): barcode, description, price, amount, match for prices, found prices.
        """
        description_str, confidence = line
        # Default values if cannot split (i.e. no price found)
        upc_barcode = ""
        descr = description_str.strip()
        price = "0.0"
        amount = 1
        # Check for decimal numbers.
        digit_regex_1 = self.config_vars.price_format[0]
        # When no price is found with digit_regex_1, check for numbers seperate by a whitespace.
        # or puncutation
        digit_regex_2 = self.config_vars.price_format[1]
        
        if search_upc_barcode:
            # Check if UPC barcode is part of the first substring.
            # Assumption: barcode at least 6 digits
            upc_search = re.search(r'^\d{4,14}',descr)
            if upc_search:
                upc_barcode = upc_search.group(0)
                descr = descr[upc_search.end(0):].strip()

        # Look for amount indicated at the start of the string
        # lower-case L and | are often confused for a 1 (one).
        starting_digits = re.search(r'^(\d|l|o|O|\|)+\s', descr)
        
        if starting_digits:
            # Replace lower-case L and | with a 1 (one)
            amount = int(starting_digits.group(0).replace("|", "1").replace("l", "1").replace("o", "0").replace("O", "0"))
            # Remove amount indicator from the string
            descr = descr[starting_digits.end(0):].strip()
        # try finding XXX.XX shaped digits without a percentage behind it.
        # other option: (\d{1,3}(?:\.|\,)\d{1,2}(?=\s))
        prices = list(re.finditer(digit_regex_1, descr))
        if len(prices) >= 1:
            price = prices[-1].group(0)
            if len(prices) == 2:
                descr = descr[:prices[-2].start()].strip()
                # If amount was not detected, see if we can use the prices to determine.
                # Only use the division if it returns an integer
                if amount == 1:
                    first_digit, second_digit = float(prices[0].group(0)), float(prices[1].group(0))
                    # We don't divide by zero
                    if first_digit != 0:
                        division = second_digit / first_digit
                        if division == int(division):
                            amount = int(division)
            else:
                descr = descr[:prices[-1].start()].strip()

        elif len(prices) == 0:
            # Sometimes the dot or comma is not seen and is instead a whitespace.
            prices = list(re.finditer(digit_regex_2, descr))
            if len(prices) > 0:
                price = prices[-1].group(0).replace(r"\s", ".").replace(r":", ".")
                descr = descr[:prices[-1].start()].strip()
        # If we want to convert to floats, dots are needed (dutch language uses a comma)
        price = price.replace(",", ".")
        # if the description still ends with a price, remove it, as it is price per unit now.
        return upc_barcode, descr, price, amount, starting_digits, prices, confidence

    def _retrieve_receipt_entry(self, items, row):
        """ Split each item line into item description and price (NL/EN).

        Args:
            items (list): List of all found items (strings)
            row (string): Current string

        Returns:
            pandas.DataFrame: Found and processed items, split into description, price, and amount.
        """

        upc_barcode, descr, price, amount, starting_digits, prices, confidence = self._find_amount_price(row)
        confidence_second = confidence
        # If the current line does not have amount or price, check the next line.
        if not starting_digits and len(prices) == 0 and len(items) > 0:
            # Look for digits with the pattern "x d.dd" as this line often describes amount x price
            check_amount_price = re.search(r'(x\s\d+(?:\.|\,)\d{2}\b)', items[0][0])
            if check_amount_price:
                # remove row from the stack
                next_row = items.pop(0)
                _, _, price, amount, _, _, confidence_second = self._find_amount_price(next_row)
        conf = (confidence + confidence_second) / 2
        # sometimes amount and description on the first line, UPC code and price on the second
        if starting_digits and len(prices) == 0 and upc_barcode == "" and len(items) > 0:
            upc_barcode_temp, _, price_temp, _, _, prices, confidence = self._find_amount_price(items[0])
            if upc_barcode_temp != "" and len(prices) == 2:
                items.pop(0)
                upc_barcode, price = upc_barcode_temp, price_temp
        return upc_barcode, descr, price, amount, conf