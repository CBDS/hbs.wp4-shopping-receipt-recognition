""" Class involving the parsing of OCR results into statistics relevant to receipts.

Written by Lanthao Benedikt (ONS) and further edited by Nick de Wolf (CBS)
"""
import re
import numpy as np
import ocr_utils
import logging

class Parser:
    """Used to parse OCR results into the requested statistics. These can be country specific.

    apply some pre-cleaning and parse receipt for date, shopname, payment mode
    and items. Items is a list of lines that need to be cleaned (use garbage
    rules) and then splitted into descr, price, and barcodes later
    """
    def __init__(self, config_vars):
        self.config_vars = config_vars

    # ------------------------------------------------------------------------------------ #
    # Parsing ...
    # ------------------------------------------------------------------------------------ #
    def extract_info(self, all_lines, lines, verbose=0):
        """Parsing the lines of the receipt.

        Parse clean text to obtain shop name, payment
        # mode, date and items list

        Args:
            all_lines (list): List of  all lines, without removal of garbage lines.
            lines (list): List of lines, without empty lines.
            verbose (int, optional): [description]. Defaults to 0.

        Returns:
            [type]: [description]
        """
        # ---------------------------------------------- #
        # Print input text for checking
        # ---------------------------------------------- #
        if verbose==1:
            ocr_utils.print_items(all_lines)
        elif verbose==2:
            ocr_utils.print_items(lines)

        all_text = [line[0] for line in all_lines]
        shop = self.parse_shop(all_text)
        date = self.parse_date(all_text)
        pmode = self.parse_payment_mode(all_text)
        items = self.parse_items(lines)
        return [shop, date, pmode, items]

    # ------------------------------------------------------------------------------------ #
    # All functions used for parsing
    # ------------------------------------------------------------------------------------ #
    def parse_date(self, lines):
        """DEPRECATED Retrieve date from the lines of a receipt.

        Args:
            lines (list): List of strings with lines of the receipt

        Returns:
            string: The found date, if any, else Unknown
        """
        date = 'Unknown' # Default value if date not found
        logging.debug("Parsing dates")
        for regex in self.config_vars.date_format:
            for line in lines:
                logging.debug('Line: %s, regex=%s'%(line,regex))
                if re.findall(regex, line):
                    date = re.findall(regex, line)[0]
                    logging.debug('\tFound: %s'%date)
        return date

    def parse_payment_mode(self, lines):
        """DEPRECATED Retrieve the payment method from the receipt.

        Use patterns as described in the config file to find the payment method.

        Args:
            lines (list): List of strings with lines of the receipt

        Returns:
            string: The found payment method, if any, else Unknown
        """
        patterns = self.config_vars.Payment_mode.items()
        logging.debug("Parsing payment method")
        [_, _, paymode] = ocr_utils.fuzzy_match(patterns, lines,
                                                       n_levels=2,
                                                       max_acc_only=1)
        if len(paymode) > 0:
            return paymode[0]
        else:
            return 'Unknown'

    def parse_shop(self, lines):
        """DEPRECATED Retrieve the shop name from the receipt.

        Use patterns as described in the config file to find the shop name.
        This is mostly direct matching and is not really used anymore.

        Args:
            lines (list): List of strings with lines of the receipt

        Returns:
            string: The found shop name, if any, else Unknown
        """
        patterns = self.config_vars.Shops.items()
        logging.debug("Searching shop name from list by fuzzy matching")
        [_, _, shopname] = ocr_utils.fuzzy_match(patterns, lines,
                                                        n_levels=2,
                                                        max_acc_only=1)
        if len(shopname) > 0:
            return shopname[0]
        else:
            return 'Unknown'

    def parse_items(self, lines):
        """Find the block of lines which contains the purchased items as printed on the receipts.

        Args:
            lines (list): List of strings with lines of the receipt

        Returns:
            list: The subset of lines which should be showing products.
        """

        lines_text = [line[0] for line in lines]

        # Identify the index of the start line for item listing
        startkeys = self.config_vars.Start_keys # Get keywords for start line
        logging.debug("Searching start of products on the receipt")
        [idx_start, start_accuracies, _] = ocr_utils.fuzzy_match(startkeys,
                                                                          lines_text,
                                                                          n_levels=1,
                                                                          max_acc_only = 0)
        # Identify the index of the stop line
        stopkeys = self.config_vars.Stop_keys # Get keywords for stop line
        logging.debug("Searching end of products on the receipt")
        [idx_stop, stop_accuracies, _] = ocr_utils.fuzzy_match(stopkeys,
                                                                       lines_text,
                                                                       n_levels=1,
                                                                       max_acc_only=0)

        accuracy_stops = np.sort(np.unique(stop_accuracies))
        # Usually the best two accuracy scores are preferable
        if len(accuracy_stops) > 2:
            idx_stop = [idx_stop[i] for i, j in enumerate(stop_accuracies) if j >= accuracy_stops[-2]]


        # Make unique value lists
        idx_start = list(set(idx_start))
        idx_stop = list(set(idx_stop))

        # Define stop index.
        # Rule 1: stop index is the smallest line index if that index is
        # not in list of start indices (because Dutch receipts have the word
        # totaal on the header line before item descriptions
        stop = len(lines) - 1 # index last line
        if bool(idx_stop): # indices found
            if min(idx_stop) in idx_start:
                idx_stop.remove(min(idx_stop))  # remove smallest value
            if idx_stop:
                stop = min(idx_stop)

        # Define start index.
        # Rule 1: start index is the max index such that it is less than stop index
        # Rule 2: I changed this rule. For Canadian receipts,
        # it was stop = max(idx_stop)
        #         I now change it to min(idx_stop) so if it finds many stop words,
        #         it takes the first one
        #         This works better for UK receipts
        start = 0
        if bool(idx_start):
            # Make a list of possible start indices such that < stop index
            idx_start_retained = [i for i in idx_start if i < stop]
            if len(idx_start_retained) > 0:
                start=max(idx_start_retained)

            # If start of items past 60% of the receipt, probably went wrong
            # Reset to 0
            if start > int(len(lines) * 0.6):
                start = 0

        # We assume at least 1 product
        if stop == start + 1:
            if idx_stop:
                idx_stop.remove(min(idx_stop))
                if idx_stop:
                    stop = min(idx_stop)
                else:
                    stop = len(lines) - 1
            else:
                stop = len(lines) - 1
        logging.debug("Pinpointing most likely start and end of products:")
        logging.debug('Start: ')
        logging.debug(f"Possible start options: {idx_start}")
        logging.debug(f"Accuracies per option: {start_accuracies}")
        logging.debug(f'Chosen start: {start}')

        logging.debug('Stop: ')
        logging.debug(f"Possible end options: {idx_stop}")
        logging.debug(f"Accuracies per option: {stop_accuracies}")
        logging.debug(f'Chosen stop: {stop}')

        return lines[start+1:stop]
