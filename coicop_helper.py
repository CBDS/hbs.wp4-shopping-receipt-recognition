""" Contains Generic functions used by multiple classes involved with COICOP codes.
Written by Lanthao Benedikt
"""

import joblib
import fasttext
import pandas as pd
from pathlib import Path
import logging

class CoicopHelper:

    @staticmethod
    def load_model(model_bin):
        """Load a model given a file path.

        Args:
            model_bin (string): File path to a trained classification model.

        Returns:
            object: The instance of the loaded model.
        """
        if Path(model_bin).is_file():
            logging.info(f'Loading pretrained model {model_bin} from drive')
            if 'FastText' in model_bin:
                model = fasttext.load_model(model_bin)
            else:
                model = joblib.load(model_bin)
            return model
        else:
            logging.info(f'Model {model_bin} can not be found')
            return None
    
    @staticmethod
    def read_data(self, filename):
        """Read the data files and return a dataframe.

        Args:
            filename (string): File name of a data file.

        Returns:
            pandas.DataFrame: The data loaded in a pandas DataFrame.
        """
        if filename.endswith('.xlsx'):
            df = pd.read_excel(filename)
        elif filename.endswith('.csv'):
            df = pd.read_csv(filename, encoding="latin-1")
        elif filename.endswith('.pkl'):
            df = pd.read_pickle(filename)
        else:
            df = pd.DataFrame()
        return df
