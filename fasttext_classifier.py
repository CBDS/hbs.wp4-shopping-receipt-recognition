import argparse
import os
import numpy as np
from datetime import datetime

import fasttext
from fasttext import train_supervised
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.metrics import f1_score
import logging

# code adapted from https://gist.github.com/aplz/537826ee96c9097a405f4c20c6164e5d (similarly to sklearn structure)


# general functions

def preprocess_labels(labels):
    """Add a prefix to the labels so that labels are formatted as 'receipt_desc __label__EFSCODE'
    :param labels: list of labels
    """
    prefix = "__label__"
    return [prefix + str(i) for i in labels]

def undo_label_preprocessing(labels):
    """ Remove prefix from one or multiple labels and cast to integer(s)
    :param labels: one or multiple labels
    """
    prefix = "__label__"
    if isinstance(labels, list) or isinstance(labels, np.ndarray):
        return [int(i.replace(prefix, '')) for i in labels]
    elif isinstance(labels, str):
        return int(labels.replace(prefix, ''))
    else:
        raise ValueError("Uknown label type")

def check_labels(labels, quick_test = True):
    """Check if labels start with __label__ as required by FastText
    :param labels: one or multiple labels
    "param quick_test: whether to test only first element if multiple labels are provided
    """
    prefix = "__label__"
    if isinstance(labels, list):
        if quick_test:
            if ~labels[0].startswith(prefix):
                return True
        else:
            for i in labels:
                if ~labels[i].startswith(prefix):
                    return True
    elif isinstance(labels, str):
        if ~labels.startswith(prefix):
            return True
    else:
        return False
    
def store_file(output_dir, features, labels):
    """Write the training data in fasttext format to disk.
    :param output_file: the name of the output file.
    :param features: the features, a list of strings.
    :param labels: the labels associated with features.
    """
    # Dump the data onto the drive because it's how FastText prefers
    with open(output_dir, 'w', encoding="utf-8", errors="ignore") as f:
        for i in range(0, len(features)):
            f.write("%s %s\n" % (labels[i], features[i]))

def save_fasttext(model, modeldir = "", fname = None):
    """ Save model to directory
    :param model: a trained fasttext model
    :param modeldir: string with existing directory
    :param fname: optional name of the model
    """
    if fname is None:
        fname = datetime.now().strftime("%Y%m%d_%H%M%S")
        filename = os.path.join(modeldir, fname + '.bin')
    else:
        filename = fname

    logging.info('\tSave model to: %s\n' % filename)
    import joblib
    # joblib.dump(model, filename)
    model.save_model(filename)


class FasttextEstimator(BaseEstimator):
    prefix = "__label__"

    def __init__(self, epoch=20, lr=1.0, wordNgrams=3, minCount=1, minn=3, maxn=12, model_dir="", verbose = 2):
        self.model_dir = model_dir
        self.model = None
        self.epoch = epoch 
        self.lr = lr
        self.wordNgrams = wordNgrams
        self.minCount =minCount
        self.minn = minn
        self.maxn = maxn
        self.verbose = verbose

    def fit(self, X, y):
        """
        Train fasttext on the given features and labels.
        :param features: a list of documents.
        :param labels: the list of labels associated with the list of features.
        """
        store_file(os.path.join(self.model_dir, "train.txt"), X, y)
        self.model = train_supervised(input="train.txt", 
                                    verbose=self.verbose,
                                    epoch=self.epoch, 
                                    lr=self.lr,
                                    wordNgrams=self.wordNgrams, 
                                    minCount=self.minCount, 
                                    minn=self.minn,
                                    maxn=self.maxn
        )
        return self

    def predict(self, X, noutputs=1):
        """ Predict the labels of the given feature
        """
        predicted_label, prob = self.model.predict(X, k=noutputs, on_unicode_error="ignore")
        return predicted_label


    def predict_proba(self, X, noutputs=1):
        """ Calculate probability of top noutputs predictions for the given feature
        """
        predicted_label, prob = self.model.predict(X, k=noutputs, on_unicode_error="ignore")
        return prob

    def save_mdl(self, fname = None):
        self.model.save_model(fname)

        # todo old:
        #save_fasttext(self.model, fname = filename)

    def get_hyperparams(self):
        """ Return hyperparamters as dict
        """
        params = {
            'epoch':self.epoch, 
            'lr':self.lr, 
            'wordNgrams':self.wordNgrams, 
            'minCount':self.minCount, 
            'minn':self.minn, 
            'maxn':self.maxn
        }
        return params

    def get_params(self, deep = False):
        return self.get_hyperparams()

    def set_params(self, **params):
        if not params:
            return self

        valid_params = self.get_params()
        for key, value in params.items():
            key, delim, sub_key = key.partition('__')
            if key not in valid_params:
                raise ValueError('Invalid parameter %s for estimator %s. '
                                 'Check the list of available parameters '
                                 'with `estimator.get_params().keys()`.' %
                                 (key, self))

            if delim:
                setattr(self, sub_key, value)
                valid_params[sub_key] = value            
            else:
                setattr(self, key, value)
                valid_params[key] = value

        return self

if __name__ == "__main__":
    import pandas as pd
    from coicop_classifier import CoicopClassifier
    from coicop_preprocessing import CoicopPreprocessing
    import coicop_classifier
    from sklearn.model_selection import StratifiedKFold

    train_val_pd = pd.read_csv("coicop_input/train_val_set.csv")
    test_pd = pd.read_csv("coicop_input/test.csv")

    coicopClassifier = coicop_classifier.CoicopClassifier(
        "input", 
        "output",
        "coicop_models/", 
        "coicop_output/",
        set_split=0.8
    )

    data_df, x_train, x_test, y_train, y_test, fasttext_train, fasttext_test = coicopClassifier.split_data(train_val_pd, remove_below_limit=5)

    # data input needs to be a little different
    # split text and labels using the latest whitespace before '__label__'
    fasttext_xtrain = []
    fasttext_ytrain = []

    for i in fasttext_train:
        x, y = i.rsplit(' ', 1)
        fasttext_xtrain.append(x)
        fasttext_ytrain.append(y)
    logging.info(fasttext_ytrain[:5])

    # fasttext_classifier
    estimator = FasttextEstimator(model_dir='.')
    fasttext_ytrain = estimator.preprocess_labels(y_train)
    logging.info(fasttext_ytrain[:5])
