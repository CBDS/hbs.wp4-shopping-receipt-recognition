""" Handles the API calls between the processing server and the backend database

Written by Nick de Wolf
"""

import json
import copy
import requests
from typing import Dict

class ApiRequests:
    """Handles the API calls between the processing server and the backend database.

    Attributes:
        config_file: Dictionary with various urls and bits of data like user/password

    """

    def __init__(self, config: str):
        """Initializes configuration file for the Api server.

        Args:
            config (str): Path to the config file.
        """

        # Requests
        with open(config) as config_file:
            request_config = json.load(config_file)
        self.config_file = request_config

    def register_new_phone(self, user_name: str, user_password: str, phone_name: str):
        """Registers a new phone to the backend.

        Registers a new phone to the backend, while also returning the details of
        the phone upon succesfully creating.

        Args:
            user_name (str): Display name of the user.
            user_password (str): Password belonging to the user.
            phone_name (str): Name of the phone/code-version.

        Returns:
            Request: The json response from the backend server.
        """

        url = self.config_file["phone_url"]
        payload = {
            "user"  : {"name" : user_name, "password" : user_password},
            "phone" : {"name" : phone_name }
        }
        return requests.post(url, json=payload)

    def pull_request(self, sync_id: int, sync_time: int):
        """Pulls a receipt from the backend.

        Gives the first receipt that has a higher syncTime than the provided syncTime.
        If there is no syncTime after the provided time,
        it resets to 0 and starts looking from there.

        Args:
            sync_id (int): The id of a specific image. Currently not used.
            sync_time (int): The syncTime after you want the first receipt.

        Returns:
            Request: The json response from the backend server.
        """
        url = self.config_file["pull_url"]
        payload = {
            "user"  : self.config_file["user"],
            "group" : self.config_file["group"],
            "phone" : self.config_file["phone"],
            "syncId" : sync_id,
            "syncTime" : sync_time
        }
        return requests.post(url, json=payload)

    def push_request(self, payload: Dict):
        """Pushes an (updated) receipt to the server with a given payload.

        Expects a payload prepared by self.return_processed_json() to be sent back to the server.

        Args:
            payload (Dict): Dictionary (json format conform backend server specs.)

        Returns:
            integer: The new syncOrder of the phone.
        """
        url = self.config_file["push_url"]
        return requests.post(url, json=payload)

    def pull_phone_request(self):
        """Returns the syncOrder of the current phone

            Returns information about the number of requests made by the phone.
            can be used to keep track if messages were sent back succesfully.

            Returns:
            integer: The syncOrder of the current phone.
        """
        url = self.config_file["phone_url"]
        payload = {
            "user" : self.config_file["user"],
            "phone": self.config_file["phone"]
        }
        backend = requests.post(url, json=payload)
        content = json.loads(backend.content)
        return content["phone"]["syncOrder"]

    def parse_content(self, request : requests.Request):
        """Parses the content of a self.pull_request()

        Parses the response from the backend server.
        Parse the content as json and find if the response contains an image.
        Also store the current syncId and syncTime to be used for the next request.

        Args:
            request (Request): Response from the backend server

        Returns:
            (dictionary, boolean, integer, integer): Content, isImagePresent, next_id, next_time
        """
        content = json.loads(request.content)
        image_present = (content["transaction"]["receiptLocation"] != "")
        next_id = content["synchronisation"]["id"]
        next_time = content["synchronisation"]["syncTime"]
        return content, image_present, next_id, next_time

    def create_product_json(self, transaction_id: str, transaction_date: str,
                            product_text: str, product_price: str,
                            coicop_text: str, coicop_code: str):
        """Parses a line from the dataframe into a dictionary that is readable for the backend API

        Formats the given input into a dictionary.
        TODO: Add more verification parts into this function.

        Args:
            transaction_id (str): Transaction_id from the original receipt
            transaction_date (str): Transaction_date from the original receipt
            product_text (str): The product description of the found product
            product_price (str): The price corresponding to the found product
            coicop_text (str): The name corresponding to the coicop code (some redundancy here)
            coicop_code (str): The coicop code corresponding to the found product

        Returns:
            dictionary: Dictionary representation for a single product line
        """
        product = {
            'transactionID'   : transaction_id,
            'product'         : product_text,
            'productCategory' : coicop_text,
            'price'           : product_price,
            'productCode'     : coicop_code,
            'productDate'     : transaction_date
        }
        return product

    def return_processed_json(self, old_json, receipt_df, unrealistic_count=36):
        """Creates a new dictionary using the initial response from the server and the new data.

        Processes the lines found in receipt_df to then process each entry into a product line.
        If products have an amount higher than 1, split it into multiple lines.

        Args:
            old_json (dictionary): Initial response from the server as found by self.pull_request()
            receipt_df (dictionary): The output from the classification pipeline
            unrealistic_count (int, optional): Maximum amount for a product before resetting to 1.
                                                Defaults to 36.

        Returns:
            dictionary: Updated response from the server, that is ready to be sent back.
        """

        new_json = copy.deepcopy(old_json)
        #new_timestamp = datetime.datetime.utcnow().timestamp()
        new_json["user"] = self.config_file["user"]
        new_json["phone"] = self.config_file["phone"]
        new_json["syncOrder"] = self.pull_phone_request() + 1
        #new_json["synchronisation"]["syncTime"] = math.floor(new_timestamp * 1000)
        new_json["synchronisation"]["syncTime"] += 1
        if 'products' not in new_json.keys():
            new_json["products"] = []
        date = new_json["transaction"]["date"]
        transaction_id = new_json["transaction"]["transactionID"]
        for _, row in receipt_df.iterrows():
            product = row.RECDESC.strip()
            coicop_code = row.Coicop_class
            if "Coicop_class_name" in row:
                coicop_name = row.Coicop_class_name
            else:
                coicop_name = "Unknown"
            coicop_prob = row.Coicop_class_prob
            amount      = row.Amount
            # Exclude unrealistic numbers
            if row.Amount > unrealistic_count:
                amount = 1
            try:
                price = float(row.Price.replace(" ", ""))
                # Round to two decimals
                price = round(price / row.Amount, 2)
            except:
                price = 0.0

            for _ in range(amount):
                new_json["products"].append(self.create_product_json(transaction_id,
                                            str(date), "_"+product, price,
                                            coicop_name, coicop_code))
        return new_json
