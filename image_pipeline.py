""" Holds the files with the general image processing pipeline
Written by Nick de Wolf
"""

import sys

from matplotlib import pyplot as plt
from matplotlib.patches import Polygon

import cv2

import image_receipt
import mrcnn.model as modellib

import logging
from receipt_enhanced import ReceiptEnhanced
from receipt_processed import ReceiptProcessed

from receipt_raw import ReceiptRaw

class InferenceConfig(image_receipt.ReceiptConfig):
    """Instance of the ReceiptConfig class with mentions of the amount of GPUs used

        Set batch size to 1 since we'll be running inference on
        one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU

        Because we only process 1 image at a time, set both to 1
    """
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1

class ImagePipeline:

    def __init__(self, root_dir, model_file, model_config=None, show_config=False):
        """Initialize the Image processing pipeline.

        Args:
            root_dir (string): Path to the root folder containing the model
            model_file (string): File name of the model
            model_config (image_receipt.ReceiptConfig, optional): Optional custom config.
            show_config (bool, optional): Print the initialized config for the mrcnn model. Defaults to False.
        """
        self.root_dir = root_dir
        # To find local version of the  mrcnn library
        sys.path.append(root_dir)
        self.model_path = model_file

        if model_config:
            self.config = model_config
        else:
            self.config = InferenceConfig()

        if show_config:
            self.config.display()

        # Create model object in inference mode.
        # Model dir is only used for training, we set it to the root folder.
        self.model = modellib.MaskRCNN(mode="inference",
                                        model_dir=self.root_dir, config=self.config)
        #self.model.keras_model.load_weights(self.model_path)
        self.model.keras_model.load_weights(self.model_path)


    @staticmethod
    def _plot_results(color_image, dilated_mask, contrasted):
        """Shows the generated polygon over the image.

        Args:
            color_image (numpy.array): Resized RGB image
            dilated_mask (numpy.array): Show the found mask, with holes filled by dilation
            contrasted (numpy.array): Contrasted gray scale image of the found receipt
        """
        _, approx = ReceiptProcessed.detect_contours_reduce(dilated_mask,
                                                                use_hough=True,
                                                                plot_results=False)
        # Create the figure of the detected area
        approx_polygon = Polygon(approx, alpha=0.4)

        fig, ax = plt.subplots(1,3)
        fig.set_figheight(15)
        fig.set_figwidth(15)
        ax[0].imshow(color_image, "gray")
        ax[1].imshow(cv2.cvtColor(color_image, cv2.COLOR_BGR2GRAY), "gray")
        ax[1].add_patch(approx_polygon)
        ax[2].imshow(contrasted, "gray")

        plt.show()

    def process_image(self, receipt: ReceiptRaw, show_image=False,
                        img_height=512, img_width=0, use_hough=True, skip_projection=False, 
                        ignore_projection_errors=False, contrast_result=True, verbose=False):
        """Processes an image to remove the background.

        Args:
            color_image ([type]): [description]
            gray_image (numpy.array): Gray scaled image
            show_image (bool, optional): Boolean to show processed image. Defaults to False.
            img_height (int, optional): Height set to determine rescaling. Defaults to 512.
            img_width (int, optional): Width set to determine rescaling. Defaults to 0.
            use_hough (bool, optional): Use hough transform to refine results. Defaults to True.

        Returns:
            (numpy.array, numpy.array, numpy.array): Resulting images.
        """
        enhanced_receipt = ReceiptEnhanced.from_raw_receipt(receipt, self.model, img_height, img_width, 
        use_hough=use_hough, skip_projection=skip_projection, ignore_projection_errors=ignore_projection_errors, 
        contrast_result=contrast_result, verbose=verbose)        

        if show_image:
            logging.info("Making a figure")
            resized_image, _ = ReceiptProcessed.rescale_image(enhanced_receipt.rgb_image, enhanced_receipt.scale_height, enhanced_receipt.scale_width)
            ImagePipeline._plot_results(resized_image, enhanced_receipt.dilated_mask, enhanced_receipt.contrasted)
        return enhanced_receipt
