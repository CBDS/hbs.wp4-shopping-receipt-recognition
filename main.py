# python .\main.py -r .\configs\request_config2.json -o .\configs\ocr_config.yml -m .\mrcnn_model -n .\FastText_ngram.bin -e .\Label_Encoder.bin -c .\CoicopNr-Naam.csv

import argparse
import os
import sys

import time
import logging
import datetime

import pandas as pd
import joblib

from receipt_processed import NoReceiptError

# OCR
from ocr_pipeline import Pipeline

# Requests
from api_requests import ApiRequests

# Image Processing
from image_pipeline import ImagePipeline

# Machine Learning
from coicop_classifier import CoicopClassifier
from coicop_helper import CoicopHelper

from cbs_server_collection import CBSServerCollection
from receipt_raw_server import ReceiptRawServer

def process_request(receipt:ReceiptRawServer, img_pipeline, receipt_pipeline, model, label_encoder, coicop_nr_name):
    """Process the content of a request and output the results in a dataframe

    Args:
        receipt (ReceiptRawServer): Content from the api request
        img_pipeline (ImagePipeline): ImagePipeline object
        receipt_pipeline (Pipeline): Pipeline object
        model (Classifier): Classifier with a .predict() function.
        label_encoder (LabelEncoder): LabelEncoder object belonging to model
        coicop_nr_name (dictionary): Dictionary with coicop ids as keys, and their names as values.

    Returns:
        pandas.Dataframe: dataframe containing the OCR, and coicop classifier results.
    """
        
    enhanced_receipt = img_pipeline.process_image(receipt, contrast_result = args["image_contrast"])
    _, _, receipt_df = receipt_pipeline.ocr_single_image(enhanced_receipt.ocr_image)

    result_df = CoicopClassifier.predict(receipt_df, model, label_encoder,
                                        "RECDESC", coicop_nr_name="CoicopNr-Naam.csv")
    return result_df

def main(args):
    sys.path.append("mrcnn")
    current_time = str(datetime.datetime.now().strftime("%Y%m%d%H%M%S"))
    logging_file = f'hbs_{current_time}.log'
    format = '%(asctime)s | %(name)s |  %(levelname)s: %(message)s'
    formatter = logging.Formatter(format)

    file_handler = logging.FileHandler(logging_file)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setLevel(logging.INFO)
    stdout_handler.setFormatter(formatter)
    
    #logging.basicConfig(handlers=[file_handler, stdout_handler], format=format, level=logging.DEBUG)
    logging.basicConfig(handlers=[file_handler, stdout_handler])

    receipt_collection = CBSServerCollection(args["request_config"])

    api = ApiRequests(args["request_config"]) # "configs/request_config.json"

    # Root directory of the project
    root_dir = os.path.abspath(args["source_folder"])
    sys.path.append(root_dir)  # To find local version of the mrcnn library
    # Local path to trained weights file
    model_path = os.path.join(root_dir, args["mask_model"]) # "./mrcnn_model/"

    img_pipeline = ImagePipeline(root_dir, model_path, show_config=False)
    ocr_configfile = args["ocr_config"] # 'configs/ocr_config.yml'

    model = CoicopHelper.load_model(args["nlp_model"])
    label_encoder = joblib.load(args["label_encoder"])
    coicop_nr_name = {row[0]:row[1] for _, row in pd.read_csv(args["coicop_names"]).iterrows()}

    # Declare an instance of class Pipeline and give it the config file
    receipt_pipeline = Pipeline(ocr_configfile)

    api.register_new_phone(
        api.config_file["user"]["name"],
        api.config_file["user"]["password"],
        api.config_file["phone"]["name"] )

    sync_id = 1
    sync_time = int(args['sync_time'] )
    logging.info("Start querying server for new receipts.")

    for receipt in receipt_collection.retrieve():
        sync_id = receipt.sync_id
        sync_time = receipt.sync_time
        print(f"Processing {sync_id}, {sync_time}")
        try:
            result_df = process_request(receipt, img_pipeline, receipt_pipeline,
                                                model, label_encoder, coicop_nr_name)
        #receipt_collection.store(receipt, result_df)

        except KeyError:
            date_time = str(datetime.datetime.now().strftime('%Y%m%d%H%M%S'))
            logging.warning(f"{date_time} Encountered problem at {sync_id}, {sync_time}:")
            logging.exception("No entry could be retrieved from database.")
            logging.info(f"Quit on error because of a database issue, check {logging_file} for details.")
            time.sleep(300)
        except NoReceiptError:
            date_time = str(datetime.datetime.now().strftime('%Y%m%d%H%M%S'))
            logging.warning(f"{date_time} Possibly could not detect a receipt at {sync_id}, {sync_time}:")
            logging.exception("Error in pipeline")
            #TODO: Only temporary, until backend accepts error codes.
            dummy_products = [['Could not detect receipt', '0.00', 0, 0, 'Unknown',
                               'Unknown', 'Unknown', '01.0.0.0.0', 'Bon', 0]]
            dummy_results = pd.DataFrame(dummy_products,
                columns= ['RECDESC', 'Price', 'Amount', 'OCR_Confidence', 'Pay-mode', 'Shopname', 'Date',
                          'Coicop_class', 'Coicop_class_name', 'Coicop_class_prob'])
            #receipt_collection.store(receipt, dummy_results)
        except Exception:
            date_time = str(datetime.datetime.now().strftime('%Y%m%d%H%M%S'))
            logging.warning(f"{date_time} Encountered problem at {sync_id}, {sync_time}:")
            logging.exception("Error in pipeline")

    logging.info(f"Finished querying server at {str(datetime.datetime.now())}.")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Parse uploaded images from the server.')
    parser.add_argument('-r', '--request_config', required=True, type=str,
                        help='The config file for the request server.')
    parser.add_argument('-o', '--ocr_config', required=True, type=str,
                        help='The config file for the OCR functionality.')
    parser.add_argument('-m', '--mask_model', required=True, type=str,
                        help='The path to the mask_rcnn model binary.')
    parser.add_argument('-n', '--nlp_model', required=True, type=str,
                        help='The path to the NLP model binary.')
    parser.add_argument('-e', '--label_encoder', required=True, type=str,
                        help='The path to the NLP Label encoder binary.')
    parser.add_argument('-c', '--coicop_names', required=True, type=str,
                        help='The path to the csv with CoicopNr to CoicopNames.')
    parser.add_argument('-s', '--source_folder', default='.', type=str,
                        help='Root folder of the project.')
    parser.add_argument('-t', '--sync_time', default=0, type=int,
                        help='Start from a certain sync_time on the server.')
    parser.add_argument('-i', '--image_contrast', default=True, type=bool,
                        help='Should the contrasted step be used.')
    parser.add_argument('-l', '--logging', default=True, type=bool,
                        help='Enable logging.')
    parser.add_argument('-w', '--wait', default=300, type=int,
                        help='The amount of seconds between calls to the server for new receipts..')

    args = vars(parser.parse_args())
    main(args)
