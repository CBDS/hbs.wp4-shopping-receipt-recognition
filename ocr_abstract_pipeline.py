""" Class involving the general OCR pipeline

Written by Lanthao Benedikt and Nick de Wolf
"""

import abc
import time
import yaml

import cv2
import pytesseract

from pytesseract import TesseractError

import pandas as pd

import ocr_data_cleaning
from ocr_parsing import Parser
from ocr_utils import Yaml2Dict
import logging

pd.set_option('display.width', 400)
pd.set_option('display.max_columns', 10)

class AbstractPipeline(abc.ABC):
    """Abstract pipeline for OCR

    Read config and receipt image file. Run OCR and call class Parser to
    Retrieve shopname, date, items descriptions and prices.

    _find_amount_price and _retrieve_receipt_entry will have to be implemented.
    """

    def __init__(self, config):
        """Initialize pipeline with config file.

        Args:
            config (string): Path to configuration file.
        """
        # Read config
        with open(config, 'r') as stream:
            docs = yaml.safe_load(stream)
            self.config_vars = Yaml2Dict(docs)
        # Initiate class Parser and give it the config file
        self.myparser = Parser(self.config_vars)

    def _determine_osd(self, image):
        image_information = pytesseract.image_to_osd(image, output_type=pytesseract.Output.DICT)
        orientation = image_information["orientation"]
        rotate = image_information["rotate"]
        confidence = image_information["orientation_conf"]
        logging.debug(f"Orientation: {orientation}, Rotate: {rotate}, Confidence: {confidence}")
        return image_information

    def cv_rotate_and_log(self, image, cv_rotate_constant, log_message):
        image = cv2.rotate(image, cv_rotate_constant)
        image_information= self._determine_osd(image)
        if image_information["orientation"] == 180 and image_information["orientation_conf"] > 0.1:
                logging.debug(log_message)
                image = cv2.rotate(image, cv2.ROTATE_180)
                image_information= self._determine_osd(image)
        image, image_information

    def _align_image(self, image):
        """Correct image to be oriented in such a way that text goes from left to right.

        We assume 90 degree angle shifts, so we only check for 90, 180, and 270 degrees rotation.

        Args:
            image (numpy.array): The image

        Returns:
            [type]: [description]
        """
        try:
            image_information= self._determine_osd(image)
        except TesseractError:
            logging.error("Failed to interpret normal image, trying 90 degree rotation")
            image = cv2.rotate(image, cv2.ROTATE_90_COUNTERCLOCKWISE)
            image_information = self._determine_osd(image)
        orientation = image_information["orientation"]
        # We skip 180 as this is too often wrongly detected and has happened rarely.
        if orientation == 270:
            self.cv_rotate_and_log(image, cv2.ROTATE_90_CLOCKWISE, "Rotated image 180 after 90 clockwise")
        elif orientation == 90:
            self.cv_rotate_and_log(image, cv2.ROTATE_90_COUNTERCLOCKWISE, "Rotated image 180 after 90 counter-clockwise")
        return image

    def get_ocr_data_from_image(self, image, languages, config):
        line_boxes_data = pytesseract.image_to_data(image, lang=languages, config=config, output_type='data.frame',
        pandas_config={'keep_default_na': False, 'na_values': ["nan"]})
        # Remove empty lines
        line_boxes_data = line_boxes_data[line_boxes_data.conf != -1]
        # Get confidence scores per box per line
        confidence_data = line_boxes_data.groupby(['block_num','par_num', 'line_num'])['conf'].apply(list)
        # Average to lines
        line_confidence_scores = [sum(line) / len(line) for line in confidence_data]

        return line_boxes_data, line_confidence_scores

    def check_180_degrees_by_confidence(self, image, languages, config, line_boxes_data, line_confidence_scores):
        if len(line_confidence_scores) > 0:
            average_confidence = sum(line_confidence_scores) / len(line_confidence_scores)
            if average_confidence < self.config_vars.ocr_confidence_threshold:
                logging.debug(f"Rotated 180, checking if confidence score of {average_confidence} improves.")
                image2 = cv2.rotate(image, cv2.ROTATE_180)
                temp_line_data, temp_scores,  = self.get_ocr_data_from_image(image2, languages, config)
                temp_average = sum(temp_scores) / len(temp_scores)
                if temp_average > average_confidence:
                    logging.debug(f"Kept rotation, average confidence score is now: {temp_average}.")
                    return image2, temp_line_data, temp_scores
        return image, line_boxes_data, line_confidence_scores

    def ocr_single_image(self, image, config=None, languages=None, verbose_parser=0, verbose_ocr=0):
        """Apply OCR to a provided image.

        Args:
            image (numpy.array): Image to be OCR'd
            config (regexp, optional): Tesseract configuration parameters. Defaults to r'--oem 3 --psm 4'.
            languages (str, optional): Language to look for on the receipt. Defaults to "eng+fra".
            verbose_parser (int, optional): Run parser verbose. Defaults to 0.
            verbose_ocr (int, optional): Run ocr verbose Defaults to 0.

        Returns:
            (numpy.array, list, pandas.DataFrame): Return the image, the detected lines,
                                                    and a DataFrame with the results.
        """
        if languages is None:
            languages = self.config_vars.ocr_language[0]
        if config is None:
            config = self.config_vars.ocr_parameters[0]

        try:
            image = self._align_image(image)
        except TesseractError as error:
            logging.error("Tesseract encountered an error while aligning the image:")
            logging.error(error)
            logging.info("Continued after tesseract error while aligning image.")

        # --------------------------------------------------- #
        # Run OCR with Tesseract and custom builder
        # --------------------------------------------------- #
        start = time.time()
        # For receipts this works best with either psm 4 or psm 6
        # TODO: Investigate the usage of the block/paragraph/line grouping for finding products (only works for psm 4)
        line_boxes_data, line_confidence_scores = self.get_ocr_data_from_image(image, languages, config)

        # Extra check using confidence values to see if image is upside-down.
        image, line_boxes_data, line_confidence_scores = self.check_180_degrees_by_confidence(image, languages, config,
                                                    line_boxes_data, line_confidence_scores)

        lines = line_boxes_data.groupby(['block_num','par_num', 'line_num'])['text'].apply(list)
        
        line_boxes = [" ".join(line).strip() for line in lines]
        # Turn into integers, as float is too specific for this use-case
        line_confidence_scores = [int(score) for score in line_confidence_scores]
        # Create a list with tuples of (LINE_OF_TEXT, SCORE)
        lines_with_confidence = list(zip(line_boxes, line_confidence_scores))
        # # Old way.
        #line_boxes = pytesseract.image_to_string(image, lang=languages, config=config).split("\n")
        #TODO: Parse confidence scores in the rest of the code now that it works.

        end = time.time()
        logging.debug('Run OCR. Time elapsed: %ds' % (end - start))

        # --------------------------------------------------- #
        # Cleaning of OCR output prior to parsing
        #
        # Call Parser class to extract shop name, date, items
        # Note: all_lines is the whole receipt - need to use
        #       that when looking for date, paymode and shop
        #       lines are receipts after we removed garbage
        #       lines, used for parsing items
        # --------------------------------------------------- #
        all_lines, lines = ocr_data_cleaning.pre_process_ocr_output(lines_with_confidence,
                                                                self.config_vars.Garbage_line_keys,
                                                                verbose=verbose_ocr)
        # print=0: no print, =1: print all_lines, =2: print trimmed lines
        [shop, date, pmode, items] = self.myparser.extract_info(all_lines, lines,
                                                                verbose=verbose_parser)
        
        # --------------------------------------------------- #
        # Make dataframe
        # --------------------------------------------------- #
        receipt_df = self.make_final_dataframe(shop, date, pmode, items)

        # --------------------------------------------------- #
        # Post-processing of final dataframe
        # --------------------------------------------------- #
        receipt_df = ocr_data_cleaning.post_process_parsed_df(receipt_df, 'RECDESC')
        return image, line_boxes, receipt_df

    def make_final_dataframe(self, shop, date, pmode, items):
        output = []
        while len(items) > 0:
            row = items.pop(0)
            upc_barcode, descr, price, amount, confidence = self._retrieve_receipt_entry(items, row)
            output.append([upc_barcode, descr, price, amount, confidence])
        df_items = pd.DataFrame(output, columns=['UPC barcode', 'RECDESC', 'Price', 'Amount', 'OCR_Confidence'])
        # Add shopname and date to final dataframe
        df_items['Pay-mode'] = pd.Series(pmode, index=df_items.index)
        df_items['Shopname'] = pd.Series(shop, index=df_items.index)
        df_items['Date'] = pd.Series(date, index=df_items.index)

        # Remove rows that have no product description.
        df_items['RECDESC'].replace("", float("NaN"), inplace=True)
        df_items.dropna(subset = ["RECDESC"], inplace=True)
        return df_items

    # ------------------------------------------------------------------------------------ #
    # All functions for splitting lines into barcode/description/price and make df
    # ------------------------------------------------------------------------------------ #
    @abc.abstractmethod
    def _find_amount_price(self, description_str, search_upc_barcode=True):
        """Find the amount and price on a line in the receipt.

        Args:
            description_str (string): Line on a receipt.

        Returns:
            (string, string, string, int, re.Match, list): barcode, description, price, amount, match for prices, found prices.
        """
        return

    @abc.abstractmethod
    def _retrieve_receipt_entry(self, items, row):
        """ Split each item line into item description and price (NL/EN).

        Args:
            items (list): List of all found items (strings)
            row (string): Current string

        Returns:
            pandas.DataFrame: Found and processed items, split into description, price, and amount.
        """
        return
