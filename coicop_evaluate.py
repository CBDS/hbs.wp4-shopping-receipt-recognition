'''Features'''
from sklearn.feature_extraction.text import TfidfVectorizer

'''Classifiers'''
from sklearn.dummy import DummyClassifier
from sklearn.linear_model import SGDClassifier  
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.multiclass import OneVsRestClassifier


'''Metrics/Evaluation'''
from sklearn.metrics import accuracy_score, confusion_matrix, \
    precision_recall_fscore_support
from sklearn.model_selection import cross_val_predict
from sklearn.pipeline import make_pipeline
from sklearn.metrics import classification_report
from sklearn.model_selection import cross_val_score

'''Plotting'''
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('darkgrid')

''' DL '''
import gensim
from gensim.models import Word2Vec
from collections import defaultdict

import pandas as pd
import numpy as np
import logging

class Coicop_Evaluator:
    """DEPRECATED Class with static methods used for evaluation of coicop classification.

    Mostly used  for the original pipeline, used in the first project report.
    Not used for the CBS API pipeline.
    """

    @staticmethod
    def show_performance(y_true, y_pred, labelnames, col, save_cm=False):
        """DEPRECATED

        Args:
            y_true ([type]): [description]
            y_pred ([type]): [description]
            labelnames ([type]): [description]
            col ([type]): [description]
            save_cm (bool, optional): [description]. Defaults to False.

        Returns:
            [type]: [description]
        """
        # classification report (note, newer version of sklearn has different report)
        logging.info(classification_report(y_true, y_pred, target_names=labelnames, digits=4))
        precision, recall, fscore, support = precision_recall_fscore_support(y_true, y_pred)

        # confusion matrix
        cm = confusion_matrix(y_true, y_pred)
        df_cm = pd.DataFrame(cm, index=labelnames, columns=labelnames)
        plt.figure(figsize=(10, 7))
        sns.set(font_scale=1.2)
        ax = sns.heatmap(df_cm, annot=True, cmap="YlGnBu", xticklabels=True, yticklabels=True)
        ax.set(xlabel="Predicted label", ylabel="True label")
        ax.figure.tight_layout()

        if save_cm:
            plt.savefig(fname = f'output/figures/cm/{col}', transparant=True)
        else:
            plt.show()

        # accuracy
        acc = accuracy_score(y_true, y_pred)
        logging.debug("Accuracy:", acc)
        logging.debug("Balanced Accuracy", np.mean(recall))

        return acc, np.mean(recall)

    @staticmethod
    def get_mistakes(labels_stripped, y_pred, labels_unique, labels_names):
        """determine case nummer, predictect value and true value for all mistakes

        DEPRECATED

        Args:
            labels_stripped (list): List of labels for the given entries
            y_pred (list): List of predictions for the given entries
            labels_unique ([type]): [description]
            labels_names ([type]): [description]

        Returns:
            [type]: [description]
        """

        indices = labels_names.keys().tolist()
        mistakes = []
        for i in range(len(y_pred)):
            if y_pred[i] != labels_stripped[i]:
                mistakes.append([indices[i], labels_unique[y_pred[i]], labels_unique[labels_stripped[i]]])
                # second value works with Kamer, check with other variables
        return mistakes
    
    @staticmethod
    def get_index_mistakes(y_true, y_pred):
        """DEPRECATED

        Args:
            y_true ([type]): [description]
            y_pred ([type]): [description]

        Returns:
            [type]: [description]
        """
        mistakes = []
        for index, (first, second) in enumerate(zip(y_true, y_pred)):
            if first != second:
                mistakes.append([index, first, second])
        return mistakes